var oldtitlepage = "ttlHome";
var oldclass = "home"
var oldbodypage = "bodyHome"

function myFunction(titlepage, bodyPage, titleclass) {
    document.getElementById(oldtitlepage).style.display = "none";
    document.getElementById(oldbodypage).style.display = "none";
    document.getElementById(oldclass).classList.remove("active");
    document.getElementById(titlepage).style.display = "block";
    document.getElementById(bodyPage).style.display = "block";
    document.getElementById(titleclass).classList.add("active");
    oldtitlepage = titlepage;
    oldclass = titleclass;
    oldbodypage = bodyPage;

}