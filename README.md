Contexte du projet

Nous vous demandons de réaliser et mettre en ligne votre portolio.

Il doit avoir au moins les éléments suivants :

    Présentation de votre parcours
    Présentation de vos compétences et des technologies que vous connaissez
    Présentation de vos projets (avec un lien vers un dépôt gitlab et, si possible, une version en ligne)
    Vos coordonnées ainsi qu'un formulaire de contact (qui fonctionne).
    En bonus, vous pouvez proposer une partie blog.

Votre portfolio doit être attractif et lisible.
IMPORTANT

L’objectif de cette évaluation est de voir si nos propositions fonctionnent pour vous, si nous devons vous accompagner plus sur certains points.

Nous n’allons pas vous noter ou juger la qualité de votre travail.

Il n'y a pas de mauvaise réponse, il n'y a pas de note, juste de la bienveillance pour vous guider dans les meilleures conditions possibles.


lien site : portfoliocadeau1.surge.sh/

